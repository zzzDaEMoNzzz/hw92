import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import './Register.css';
import {registerUser} from "../../store/actions/usersActions";

class Register extends Component {
  state = {
    username: '',
    password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.registerUser(this.state);
  };

  render() {
    return (
      <Fragment>
        <h2>Register</h2>
        <form className="Login" onSubmit={this.submitFormHandler}>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            id="username"
            name="username"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            autoFocus
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
          />
          <button>Register</button>
        </form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(null, mapDispatchToProps)(Register);