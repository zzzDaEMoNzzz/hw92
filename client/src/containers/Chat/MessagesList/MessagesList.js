import React, {Component, createRef} from 'react';
import './MessagesList.css';

class MessagesList extends Component {
  state = {
    message: ''
  };

  messagesList = createRef();

  inputChangeHandler = event => {
    this.setState({...this.state, message: event.target.value});
  };

  formSubmitHandler = event => {
    event.preventDefault();

    this.props.sendMessage(this.state.message);
    this.setState({message: ''});
  };

  scrollToBottom = async (duration = 100) => {
    const delay = ms => {
      return new Promise(resolve => setTimeout(resolve, ms));
    };

    const startPoint = this.messagesList.current.scrollTop;
    const endPoint = this.messagesList.current.scrollHeight;

    const difference = endPoint - startPoint;

    const interactions = 100;

    for (let i = 0; i < interactions; i++) {
      this.messagesList.current.scrollTop = startPoint + (difference / interactions * i);
      await delay(duration / interactions);
    }
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.messages !== this.props.messages) {
      this.scrollToBottom();
    }
  }

  render() {
    return (
      <div className="MessagesList">
        <h3>Chat room</h3>
        <div className="MessagesList-messagesWrapper">
          <div className="MessagesList-messages" ref={this.messagesList}>
            {this.props.messages.map(message => (
              <div key={message._id} className="MessagesList-message">
                <span className="MessagesList-author">{message.user.username}:</span>
                <span>{message.message}</span>
                {this.props.user.role === 'moderator' && (
                  <button className="MessagesList-deleteBtn" onClick={() => this.props.deleteMessage(message._id)}>x</button>
                )}
              </div>
            ))}
          </div>
        </div>
        <form className="MessagesList-form" onSubmit={this.formSubmitHandler}>
          <input
            type="text"
            value={this.state.message}
            onChange={this.inputChangeHandler}
            autoFocus
            required
          />
          <button>Send</button>
        </form>
      </div>
    );
  }
}

export default MessagesList;