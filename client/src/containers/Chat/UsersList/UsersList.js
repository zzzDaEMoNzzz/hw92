import React from 'react';
import './UsersList.css';

const UsersList = ({usernames}) => {
  return (
    <div className="UsersList">
      <h3>Online users</h3>
      <div className="UsersList-users">
        {usernames.map(username => (
          <div key={username}>{username}</div>
        ))}
      </div>
    </div>
  );
};

export default UsersList;
