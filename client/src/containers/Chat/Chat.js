import React, {Component} from 'react';
import {connect} from "react-redux";
import './Chat.css';
import UsersList from "./UsersList/UsersList";
import MessagesList from "./MessagesList/MessagesList";
import {connectionOpen} from "../../store/actions/chatActions";

class Chat extends Component {
  componentDidMount() {
    this.props.connectionOpen();
  }

  sendMessage = message => {
    this.props.websocket.send(JSON.stringify({
      type: "NEW_MESSAGE",
      message
    }));
  };

  deleteMessage = messageID => {
    this.props.websocket.send(JSON.stringify({
      type: "MESSAGE_DELETED",
      messageID
    }));
  };

  render() {
    const uniqueUsernames = this.props.activeConnections.reduce((arr, conn) => {
      const username = conn.username;

      if (!arr.includes(username)) {
        arr.push(username);
      }

      return arr;
    }, []);

    return (
      <div className="Chat">
        <UsersList usernames={uniqueUsernames}/>
        <MessagesList
          sendMessage={this.sendMessage}
          messages={this.props.messages}
          user={this.props.user}
          deleteMessage={this.deleteMessage}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  websocket: state.chat.websocket,
  activeConnections: state.chat.activeConnections,
  messages: state.chat.messages
});

const mapDispatchToProps = dispatch => ({
  connectionOpen: () => dispatch(connectionOpen())
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);