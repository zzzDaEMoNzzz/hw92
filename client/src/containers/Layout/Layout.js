import React, {Component} from 'react';
import {connect} from "react-redux";
import './Layout.css';
import {logoutUser} from "../../store/actions/usersActions";
import Header from "../../components/Header/Header";

class Layout extends Component {
  render() {
    return (
      <div className="Layout">
        <Header user={this.props.user} logoutUser={this.props.logoutUser}/>
        <div className="Layout-body">
          {this.props.children}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);