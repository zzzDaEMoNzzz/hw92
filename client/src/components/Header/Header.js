import React from 'react';
import './Header.css';
import {Link, NavLink} from "react-router-dom";

const Header = ({user, logoutUser}) => {
  const anonMenu = (
    <nav>
      <NavLink to="/register" exact>Register</NavLink>
      <span>or</span>
      <NavLink to="/login" exact>Login</NavLink>
    </nav>
  );

  const userMenu = (
    <nav>
      <span className="Header-splitter">Hello, {user && user.username}!</span>
      <Link to="/" onClick={logoutUser}>Logout</Link>
    </nav>
  );

  return (
    <div className="Header">
      <NavLink to="/" exact>Chat</NavLink>
      {user ? userMenu : anonMenu}
    </div>
  );
};

export default Header;
