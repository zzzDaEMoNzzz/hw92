import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';
import {CONNECTION_CLOSE} from "./chatActions";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';

const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          try {
            const errorsObject = error.response.data.errors;
            const errorMessage = errorsObject[Object.keys(errorsObject)[0]].message;

            NotificationManager.error(errorMessage);
          } catch (e) {
            NotificationManager.error('Invalid data');
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    )
  }
};

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data));
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          try {
            const errorsObject = error.response.data.errors;
            const errorMessage = errorsObject[Object.keys(errorsObject)[0]].message;

            NotificationManager.error(errorMessage);
          } catch (e) {
            NotificationManager.error('Wrong login or password');
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  }
};

export const LOGOUT_USER = 'LOGOUT_USER';

export const logoutUser = () => {
  return dispatch => {
    return axios.delete('/users/sessions').then(
      () => {
        dispatch({type: LOGOUT_USER});
        dispatch({type: CONNECTION_CLOSE});
        NotificationManager.success('Logged out!');
      },
      () => {
        NotificationManager.error('Could not logout');
      }
    );
  };
};

export const checkAuth = () => {
  return dispatch => {
    axios.get('/users/sessions').then(
      null,
      error => {
        if (error.response) {
          dispatch({type: LOGOUT_USER})
        } else {
          NotificationManager.error('Failed to connect to server');
        }
      }
    );
  };
};