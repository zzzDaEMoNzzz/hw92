import {apiWsURL} from "../../constants";

export const CONNECTION_OPEN = 'CONNECTION_OPEN';
export const CONNECTION_CLOSE = 'CONNECTION_CLOSE';

export const connectionOpen = () => {
  return (dispatch, getState) => {
    const user = getState().users.user;
    const token = user && user.token;

    if (token) {
      const apiUrl = `${apiWsURL}/chat?token=${token}`;

      const connection = new WebSocket(apiUrl);

      connection.onmessage = event => {
        const message = JSON.parse(event.data);
        dispatch(message);
      };

      connection.onclose = () => {
        setTimeout(() => connectionOpen(), 5000);
      };

      connection.onopen = () => {
        dispatch({type: CONNECTION_OPEN, connection});
      };
    }
  };
};