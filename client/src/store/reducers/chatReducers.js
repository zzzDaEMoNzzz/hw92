import {CONNECTION_CLOSE, CONNECTION_OPEN} from "../actions/chatActions";

const initialState = {
  websocket: null,
  activeConnections: [],
  messages: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CONNECTION_OPEN:
      return {...state, websocket: action.connection};
    case CONNECTION_CLOSE:
      state.websocket.close();
      return {...state, websocket: null};
    case 'ACTIVE_USERS':
      return {...state, activeConnections: action.activeConnections};
    case 'USER_CONNECTED':
      return {
        ...state,
        activeConnections: [
          ...state.activeConnections,
          action.conn
        ]
      };
    case 'USER_DISCONNECTED':
      const activeConnections = state.activeConnections.filter(conn => conn._id !== action.connID);
      return {...state, activeConnections: activeConnections};
    case 'LATEST_MESSAGES':
      return {...state, messages: action.messages.reverse()};
    case 'NEW_MESSAGE':
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case 'MESSAGE_DELETED':
      const messages = state.messages.filter(message => message._id !== action.messageID);
      return {
        ...state,
        messages
      };
    default:
      return state;
  }
};

export default reducer;