import React, {Component} from 'react';
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import 'react-notifications/lib/notifications.css';
import './App.css';
import {checkAuth} from "./store/actions/usersActions";
import Layout from "./containers/Layout/Layout";
import Routes from "./Routes";

class App extends Component {
  componentDidMount() {
    if (this.props.user) {
      this.props.checkAuth();
    }
  }

  render() {
    return (
      <div className="App">
        <NotificationContainer/>
        <Layout>
          <Routes user={this.props.user}/>
        </Layout>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  checkAuth: () => dispatch(checkAuth())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
