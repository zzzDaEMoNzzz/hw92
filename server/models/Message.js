const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  message: {
    type: String,
    required: true
  },
  datetime: {
    type: String,
    required: true
  },
  hidden: {
    type: Boolean,
    required: true,
    default: false
  }
});

const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;