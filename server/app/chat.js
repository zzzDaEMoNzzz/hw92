const express = require('express');
const nanoid = require('nanoid');
const User = require('../models/User');
const Message = require('../models/Message');

const router = express.Router();

const activeConnections = {};

router.ws('/', async (ws, req) => {
  const token = req.query.token;

  if (!token) {
    return ws.close();
  }

  const user = await User.findOne({token}).lean();

  if (!user) {
    return ws.close();
  }

  const id = nanoid();
  console.log(`Client connected! id=${id}`);
  activeConnections[id] = {
    user: {
      _id: user._id,
      username: user.username
    },
    ws
  };

  Object.values(activeConnections).map(conn => {
    if (conn.user.username !== user.username) {
      conn.ws.send(JSON.stringify({
        type: 'USER_CONNECTED',
        conn: {
          _id: id,
          username: user.username
        }
      }));
    }
  });

  const activeUsers = Object.keys(activeConnections).map(connID => {
    const conn = activeConnections[connID];

    return {
      _id: connID,
      username: conn.user.username
    };
  });

  ws.send(JSON.stringify({
    type: 'ACTIVE_USERS',
    activeConnections: activeUsers
  }));

  ws.send(JSON.stringify({
    type: 'LATEST_MESSAGES',
    messages: await Message.find({hidden: false}).populate('user', ['_id', 'username']).sort({'datetime': -1}).limit(30)
  }));

  ws.on('message', async msg => {
    let decodedMessage;

    try {
      decodedMessage = JSON.parse(msg);
    } catch (e) {
      return console.log('Not a valid message');
    }

    switch (decodedMessage.type) {
      case 'NEW_MESSAGE':
        const message = new Message({
          user: user._id,
          message: decodedMessage.message,
          datetime: new Date().toISOString()
        });

        await message.save();

        Object.keys(activeConnections).forEach(connID => {
          const conn = activeConnections[connID];

          conn.ws.send(JSON.stringify({
            type: 'NEW_MESSAGE',
            message: {
              _id: message._id,
              message: message.message,
              datetime: message.datetime,
              user: {
                _id: user._id,
                username: user.username
              }
            }
          }));
        });
        break;
      case 'MESSAGE_DELETED':
        if (user.role === 'moderator') {
          const message = await Message.findById(decodedMessage.messageID);
          message.hidden = true;

          await message.save();

          Object.keys(activeConnections).forEach(connID => {
            const conn = activeConnections[connID];

            conn.ws.send(JSON.stringify({
              type: 'MESSAGE_DELETED',
              messageID: decodedMessage.messageID
            }));
          });
        }
        break;
      default:
        console.log('Not valid message type,', decodedMessage.type);
    }
  });

  ws.on('close', msg => {
    console.log(`Client disconnected! id=${id}`);

    Object.keys(activeConnections).map(connID => {
      const conn = activeConnections[connID];

      if (connID !== id) {
        conn.ws.send(JSON.stringify({
          type: 'USER_DISCONNECTED',
          connID: id
        }));
      }
    });

    delete activeConnections[id];
  });
});

module.exports = router;