const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Message = require('./models/Message');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [user, moderator] = await User.create(
    {
      username: 'user',
      password: '123',
      role: 'user',
      token: nanoid()
    },
    {
      username: 'moderator',
      password: '123',
      role: 'moderator',
      token: nanoid()
    }
  );

  await Message.create(
    {
      user: user._id,
      message: '1',
      datetime: new Date().toISOString()
    },
    {
      user: user._id,
      message: '2',
      datetime: new Date().toISOString()
    },
    {
      user: moderator._id,
      message: '3',
      datetime: new Date().toISOString()
    },

  );

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});