const express = require('express');
const expressWs = require('express-ws');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require("./config");

const app = express();
expressWs(app);

const users = require('./app/users');
const chat = require('./app/chat');

app.use(cors());
app.use(express.json());

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/users', users);
  app.use('/chat', chat);

  app.listen(config.port, () => {
    console.log(`[Server] started on ${config.port} port`);
  });
});
