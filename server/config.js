module.exports = {
  port: 8000,
  dbUrl: 'mongodb://localhost/chat',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  }
};
